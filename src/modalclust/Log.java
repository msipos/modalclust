package modalclust;

/** Just a simple logger. */
public class Log {
  public enum Destination {
    NONE, STDERR;
  }


  public static Destination destination = Destination.NONE;


  public static void log(String format, Object... args) {
    if (destination == Destination.STDERR) {
      System.err.printf(format, args);
      System.err.println();
    }
  }
}
