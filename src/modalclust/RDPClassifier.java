package modalclust;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class RDPClassifier {
  private static void parseClassification() {
    final File f = ModalClust.params.classFile;
    final String level = ModalClust.params.classLevel;
    try {
      final BufferedReader br = new BufferedReader(new FileReader(f));

      String line;
      while ((line = br.readLine()) != null) {
        line = line.trim();
        if (line.length() == 0) continue;

        String[] arr = line.split("\t+");

      }

    } catch (FileNotFoundException ex) {
      System.err.println("No such file: " + f.getAbsolutePath());
      System.exit(1);
    } catch (IOException ex) {
      System.err.println("IO error while reading classification file: " + f.getAbsolutePath());
      System.exit(1);
    }
  }

}
