package modalclust;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import modalclust.seq.Sequence;
import modalclust.seq.Snippet;


public class ClusteringQuality {
  /**
   * Clusters are a list of lists of strings (sequence ids).
   */
  public static List<List<Snippet>> loadClusters(File f, Map<String, Snippet> snippetMap) {
    final List<List<Snippet>> clusters = new ArrayList<List<Snippet>>();

    // Load only 1 line of the file.
    String firstLine = null;
    {
      try {
        final BufferedReader br = new BufferedReader(new FileReader(f));

        firstLine = br.readLine();

        br.close();
      } catch (IOException ex) {
        System.err.println("Error while reading: " + f.getAbsolutePath() + " : " + ex.getMessage());
        System.exit(1);
      }
    }
    if (firstLine == null) {
      System.err.println("Error while reading: " + f.getAbsolutePath() + " : no lines");
      System.exit(1);
    }

    final String[] splitLine = firstLine.split("\\s");

    if (splitLine.length <= 2) {
      System.err.println("Error while reading: " + f.getAbsolutePath() + " : invalid format");
      System.exit(1);
    }
    System.err.println("Using clustering for radius " + splitLine[0] + " with " + splitLine[1] + " clusters.");

    for (int i = 2; i < splitLine.length; i++) {
      final String clusterTogether = splitLine[i];

      final String[] ids = clusterTogether.split(",");

      // Convert ids into clusterSnippets
      final Snippet[] snippets = new Snippet[ids.length];
      for (int j = 0; j < ids.length; j++) {
        snippets[j] = snippetMap.get(ids[j]);
      }
      final List<Snippet> clusterSnippets = Arrays.asList(snippets);
      clusters.add(clusterSnippets);
    }

    if (clusters.size() != Integer.parseInt(splitLine[1])) {
      System.err.println("Mismatched number of clusters in the file.");
      System.exit(1);
    }

    return clusters;
  }


  public static double calculateClusterDifference(final List<Snippet> cluster1, final List<Snippet> cluster2,
                                                  final Clustering.DistanceMetric<Snippet> distMetric) {
    double difference = 0.0;
    int num = 0;

    for (final Snippet sn1 : cluster1) {
      for (final Snippet sn2 : cluster2) {
        difference += distMetric.distance(sn1, sn2, 1f);
        num += 1;
      }
    }

    return difference / num;
  }

  public static void calculateCH(final List<Sequence> seqs, File clusteringFile) {
    //  This will be the distance metric that we use:
    final Clustering.DistanceMetric<Snippet> distMetric = new Clustering.DistanceMetric<Snippet>() {
      @Override public float distance(Snippet e1, Snippet e2, float maxRadius) {
        return DistanceMetrics.distanceMothur(e1, e2, 1f);
      }
    };
    final Map<String, Snippet> seqsMap = Dereplicate.getAllIdMap(seqs);
    final List<List<Snippet>> clusters = loadClusters(clusteringFile, seqsMap);

    // Calinski Harabasz formula:
    //   (n-k)/(k-1) sum_KL (xb_K - xb_L) / sum_K sum_ij (x_i - x_j)
    // where n = # of elements, k = # of clusters

    // Do the top sum:
    System.err.println("Doing top sum (inter-cluster distances):");
    double topSum = 0.0;
    int n = 0;
    final int kk = clusters.size();
    System.err.println("k = ");
    System.out.println(kk);

    int modder = kk / 30; if (modder < 1) modder = 1;
    long interCounter = 0;

    for (int K = 0; K < kk; K++) {
      if ((K % modder) == 0) {
        System.err.println("Calculating cluster distances for cluster " + K + "...");
      }
      final List<Snippet> clusterK = clusters.get(K);
      n += clusterK.size();

      for (int L = K+1; L < kk; L++) {

        final List<Snippet> clusterL = clusters.get(L);
        topSum += calculateClusterDifference(clusterK, clusterL, distMetric);
        interCounter++;
      }
    }
    System.err.println("n = ");
    System.out.println(n);
    System.err.println("Top sum = ");
    System.out.println(topSum);
    System.err.println("Number of inter-distances = " + interCounter);

    // Do the bottom sum:
    System.err.println("Doing bottom sum (in-cluster distances):");
    double bottomSum = 0.0;
    long inCounter = 0;
    for (int K = 0; K < kk; K++) {
      final List<Snippet> clusterK = clusters.get(K);
      for (int i = 0; i < clusterK.size(); i++) {
        final Snippet snI = clusterK.get(i);
        for (int j = i+1; j < clusterK.size(); j++) {
          final Snippet snJ = clusterK.get(j);
          bottomSum += distMetric.distance(snI, snJ, 1f);
          inCounter++;
        }
      }
    }

    System.err.println("Bottom sum = ");
    System.out.println(bottomSum);

    System.err.println("Number of in-distances = " + inCounter);

    System.err.println("Total = ");
    System.out.println(
                        ((double) (n - kk)) / ((double) (kk - 1)) * topSum / bottomSum
                      );
  }
}
