package modalclust;

import modalclust.seq.Snippet;
import modalclust.seq.Sequence;
public class SimpleDistanceMetric extends Clustering.DistanceMetric<Sequence> {
  @Override public float distance(Sequence da, Sequence db, float maxRadius) {
    final Snippet a = da.snippet, b = db.snippet;

    int distance = 0;
    int number = 0;

    if (a.size() != b.size()) {
      throw new RuntimeException("asked for distance between two snippets of differing lengths");
    }

    final int size = a.size();

    for (int i = 0; i < size; i++) {
      final byte b1 = a.data[i];
      final byte b2 = b.data[i];
      if (b1 != b2) {
        number++;
        distance++;
      } else {
        if (b1 != '-') { // Two gaps don't count towards the total number
          number++;
        }
      }
    }

    return (float) (((double) distance) / ((double) number));
  }
}
