package modalclust;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import modalclust.seq.Sequence;
import modalclust.seq.Snippet;

public class Fasta {

  public static List<Sequence> readFasta(File f) throws FileNotFoundException {
    ArrayList<Sequence> list = new ArrayList<Sequence>();
    String id = null;
    String seq = null;

    {
      BufferedReader br = new BufferedReader(new FileReader(f));
      String line;
      try {
        while ((line = br.readLine()) != null) {
          line = line.trim();
          if (line.length() == 0) continue;
          if (line.charAt(0) == '>') {
            if (seq != null) {
              list.add(new Sequence(new Snippet(seq), id));
            }

            id = line.substring(1);
            seq = "";
            continue;
          }

          if (seq == null) throw new FastaError(String.format("file '%s' is not in proper FASTA format", f.getName()));
          seq += line;
        }
      } catch (IOException ex) {
        throw new FastaError(String.format("IO error while reading '%s", f.getName()), ex);
      }
    }

    // Last sequence
    if (seq != null) {
      list.add(new Sequence(new Snippet(seq), id));
    }

    return list;
  }
}
