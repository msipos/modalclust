package modalclust;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import modalclust.seq.Sequence;
import modalclust.seq.Snippet;
public class Dereplicate {
  public static Map<String, String[]> loadDict(File dictFile) {
    Map<String, String[]> m = new HashMap<String, String[]>();

    try {
      BufferedReader br = new BufferedReader(new FileReader(dictFile));
      String line;
      int numLine = 0;
      while ((line = br.readLine()) != null) {
        numLine++;
        line = line.trim();
        String arr[] = line.split("\\s");
        if (arr.length != 2) {
          System.out.printf("Line %d in the dictionary/names file '%s' is corrupted!%n", numLine, dictFile.getAbsolutePath());
          System.out.println("Line is:");
          System.out.println(line);
          System.exit(1);
        }
        m.put(arr[0], arr[1].split(","));
      }
    } catch (FileNotFoundException ex) {
      System.out.println("ERROR: Cannot find '" + dictFile.getName() + "'");
      System.exit(1);
    } catch (IOException ex) {
      System.out.println("ERROR: IO error while reading '" + dictFile.getName() + "'");
      System.exit(1);
    }

    return m;
  }


  public static Map<String, Snippet> makeMap(List<Sequence> list) {
    Map<String, Snippet> map = new HashMap<String, Snippet>();
    for (Sequence seq : list) {
      map.put(seq.getId(), seq.snippet);
    }
    return map;
  }


  public static List<Sequence> autoDereplicate(List<Sequence> list) {
    Map<Snippet, Sequence> map = new HashMap<Snippet, Sequence>();
    List<Sequence> dlist = new ArrayList<Sequence>();
    for (Sequence seq : list) {
      if (map.containsKey(seq.snippet)) {
        map.get(seq.snippet).ids.add(seq.getId());
      } else {
        Sequence dseq = new Sequence(seq.snippet, seq.getId());
        map.put(seq.snippet, dseq);
        dlist.add(dseq);
      }
    }
    return dlist;
  }


  public static Map<String, Snippet> getAllIdMap(List<Sequence> dlist) {
    final Map<String, Snippet> map = new HashMap<String, Snippet>();

    for (final Sequence seq : dlist) {
      for (final String id : seq.ids) {
        map.put(id, seq.snippet);
      }
    }
    return map;
  }


  public static List<Sequence> dereplicate(ModalClustParams params, List<Sequence> list) {
    // Produce duplication list (or deduplicate)
    List<Sequence> dlist = null;

    if (params.dictFile != null) {
      // If dictFile is given then use it
      Map<String, Snippet> idMap = makeMap(list);
      Map<String, String[]> dictMap = loadDict(params.dictFile);
      dlist = new ArrayList<Sequence>();

      for (Map.Entry<String, String[]> entry : dictMap.entrySet()) {
        String name = entry.getKey();
        String[] value = entry.getValue();
        Snippet snip = idMap.get(value[0]);

        if (snip == null) {
          // This means that this sequence got removed in the alignment step.  In this case, we just
          // ignore this sequence from the clustering process.
          continue;
        }

        Sequence dseq = new Sequence(snip);
        for (String sub: value) {
          dseq.ids.add(sub);
        }
        dlist.add(dseq);
      }
    } else {
      // Otherwise, automatically dereplicate
      dlist = autoDereplicate(list);
      System.err.println("INFO: Automatically dereplicated from " + list.size() + " to " + dlist.size());
    }
    return dlist;
  }
}
