package modalclust;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

public class ModalClustCLI {
  public static void parseArguments(String[] args) {
    JCommander jcommander = new JCommander(ModalClust.params);
    jcommander.setProgramName("java -jar ModalClust.jar");

    try {
      jcommander.parse(args);
    } catch (ParameterException e) {
      System.out.println(e.getMessage());
      System.out.println();
      jcommander.usage();
      System.exit(1);
    }
  }
}
