package modalclust.seq;


import java.util.Arrays;

public class Snippet {
  public final byte[] data;

  // Indices of the first and last non-gaps in the sequence.
  public final int xl, xr;

  // Number of non-gaps in the sequence.
  public final int l;

  // Number of A, C, G and T in the sequence
  public final int na, nc, ng, nt;

  // Number of gap regions
  public final int ngr;

  public Snippet(String seq) {
    data = new byte[seq.length()];
    boolean passedFirstNonGap = false;
    int txl = 0, txr = 0;
    int tl = 0;
    int tna = 0, tnc = 0, tng = 0, tnt = 0;
    int tngr = 0;

    // Copy into the array while also computing xl and xr
    for (int i = 0; i < seq.length(); i++) {
      byte letter = (byte) seq.charAt(i);
      if (letter == '.') {
        letter = '-'; // Make all gaps uniform
      }
      if (Character.isLowerCase(letter)) {
        letter = (byte) Character.toUpperCase(letter);
      }

      if (letter != '-') {
        tl++;

        // Not a gap:
        if (!passedFirstNonGap) {
          passedFirstNonGap = true;
          txl = i;
        }
        txr = i;
      }

      if (letter == 'A') tna++;
      if (letter == 'C') tnc++;
      if (letter == 'G') tng++;
      if (letter == 'T') tnt++;

      data[i] = letter;
    }

    // Count how many gap regions between xl and xr.
    for (int i = txl; i < txr; i++) {
      final byte b1 = data[i], b2 = data[i+1];
      if (b1 != '-' && b2 == '-') tngr++;
    }

    ngr = tngr;
    na = tna; nc = tnc; ng = tng; nt = tnt;
    xl = txl; xr = txr;
    l = tl;
  }

  public int size() {
    return data.length;
  }

  public boolean isReasonable() {
    for (byte b : data) {
      if (!Character.isLetterOrDigit((char) b)) {
        if (b != '-') {
          return false;
        }
      }
    }
    return true;
  }

  @Override
  public String toString() {
    StringBuilder b = new StringBuilder("Snpt(");

    int upto = data.length;
    boolean trimmed = false;
    if (upto > 10) {
      upto = 8;
      trimmed = true;
    }
    for (int i = 0; i < upto; i++) {
      b.append((char) data[i]);
    }
    if (trimmed) {
      b.append("..");
    }

    b.append(")");

    return b.toString();
  }

  public String getSequence() {
    StringBuilder b = new StringBuilder();

    for (final byte c : data) {
      b.append((char) c);
    }
    return b.toString();
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(data);
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof Snippet && Arrays.equals(((Snippet) o).data, data);
  }
}
