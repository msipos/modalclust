package modalclust.seq;

import java.util.HashSet;
import java.util.Iterator;

/** A Sequence is a Snippet + any number of unique ids. */
public class Sequence implements Comparable<Sequence> {
  public final Snippet snippet;
  public final HashSet<String> ids;

  /** Constructor with no ids. */
  public Sequence(Snippet snippet) {
    assert snippet != null;

    this.snippet = snippet;
    this.ids = new HashSet<String>();
  }


  /** Constructor with 1 id is commonly used. */
  public Sequence(Snippet snippet, String id) {
    this(snippet);
    ids.add(id);
  }


  /** Some sequences have exactly one id.  For those use this method: */
  public String getId() {
    assert ids.size() == 1;

    final Iterator<String> iter = ids.iterator();
    return iter.next();
  }


  @Override
  public String toString() {
    StringBuilder b = new StringBuilder("DuplSequence(numIds = ");
    b.append(ids.size());
    b.append(", snpt = ");
    b.append(snippet.toString());
    b.append(")");
    return b.toString();
  }

  public int compareTo(Sequence other) {
    return this.ids.size() - other.ids.size();
  }
}
