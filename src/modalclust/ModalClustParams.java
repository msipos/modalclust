package modalclust;

import com.beust.jcommander.Parameter;
import java.io.File;

public class ModalClustParams {
  @Parameter(names = {"-f", "--fasta"}, required=true, description="Fasta file to cluster/compute quality")
  File fastaFile;


  @Parameter(names = {"-r", "--radius"}, required=false, description="Clustering radius (0.0 to 1.0)")
  double radius = 0.03;


  @Parameter(names = {"-d", "--dict"}, required=false, description="Dictionary file")
  File dictFile;


  @Parameter(names = {"-p", "--prefix"}, required=false, description="Prefix of output files")
  String filePrefix = "cluster";


  @Parameter(names = {"-c", "--classificationFile"}, required=false, description="Classification file from RDP classifier")
  File classFile;


  @Parameter(names = {"-l", "--classificationLevel"}, required=false, description="Level of classification (genus, phylum, etc.)")
  String classLevel = "genus";


  @Parameter(names = {"-v", "--verbose"}, required=false, description="Show verbose output.")
  boolean verbose = false;


  @Parameter(names = {"-q", "--quality"}, required=false, description="Compute quality of the following clustering file.")
  File clustQualityFile;


  @Parameter(names = {"-n", "--noOptimization"}, required=false, description="If set, do not compute optimization of clusters.")
  boolean noOptimization = false;
}