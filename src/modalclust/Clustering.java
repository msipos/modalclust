package modalclust;

import java.util.*;

/** Clustering is a a temporary object that performs a clustering.  */
public class Clustering<T> {
  public static class Cluster<T> {
    /** Direct access to the elements. */
    public final Set<T> elements = new HashSet<T>();


    /** Representative sequence (May be used by the clustering algorithm). */
    T representative;


    Cluster(T element) {
      representative = element;
      elements.add(element);
    }
  }


  public static abstract class DistanceMetric<T> {
    public abstract float distance(T e1, T e2, float maxRadius);
  }


  private final List<Cluster<T>> clusters = new ArrayList<Cluster<T>>();


  public Collection<Set<T>> getClusters() {
    final Collection<Set<T>> rv = new ArrayList<Set<T>>();
    for (Cluster<T> cluster : clusters) {
      rv.add(cluster.elements);
    }
    return rv;
  }


  public Collection<T> getSeeds() {
    final Collection<T> rv = new ArrayList<T>();
    for (Cluster<T> cluster : clusters) {
      rv.add(cluster.representative);
    }
    return rv;
  }


  /** Cluster the elements using "radius clustering". An element is added to a cluster if it is within the radius
   * of a previous cluster.  If it is not, then a new cluster is seeded.  The elements are clustered in the order
   * of the collection.  At the end, an optimization step is performed, where elements are added to their nearest
   * center. */
  public void radiusCluster(Collection<T> elements, DistanceMetric<T> metric, float radius, boolean noOptimization) {
    int counter = 0;
    int multiplier = elements.size() / 50;
    if (multiplier == 0) multiplier = 1;

    main_loop:
    for (T element : elements) {
      if (counter % multiplier == 0) {
        Log.log("Clustering %d %% complete.", counter / multiplier * 2);
      }
      counter++;


      // First try to tack it to one of the existing clusters
      for (Cluster<T> cluster : clusters) {
        final T center = cluster.representative;
        final float distance = metric.distance(element, center, radius);

        if (distance < radius) {
          cluster.elements.add(element);
          continue main_loop;
        }
      }

      // Otherwise make a new cluster
      Cluster<T> cluster = new Cluster<T>(element);
      clusters.add(cluster);
    }

    if (noOptimization) return;

    // Now, iterate over all the non-center elements and migrate them to the other centers.
    counter = 0;
    multiplier = clusters.size() / 50;
    if (multiplier == 0) multiplier = 1;


    for (int originIndex = 0; originIndex < (clusters.size()-1); originIndex++) {
      final Cluster<T> originCluster = clusters.get(originIndex);

      if (counter % multiplier == 0) {
        Log.log("Optimizing ~%d %% complete.", counter / multiplier * 2);
      }
      counter++;

      // Now, go through the elements of this cluster:
      for (final Iterator<T> iter = originCluster.elements.iterator(); iter.hasNext(); ) {
        final T element = iter.next();

        if (!element.equals(originCluster.representative)) {
          // This element is a non-center element.
          // Now, iterate over all of the centers, and find if there is a better place for this element.
          float minDistance = metric.distance(element, originCluster.representative, radius);
          Cluster<T> closestCluster = originCluster;

          for (int anotherIndex = originIndex+1; anotherIndex < clusters.size(); anotherIndex++) {
            final Cluster<T> anotherCluster = clusters.get(anotherIndex);
            final float distance = metric.distance(element, anotherCluster.representative, radius);
            if (distance < minDistance) {
              minDistance = distance;
              closestCluster = anotherCluster;
            }
          }

          // If there is a better place for this element, then move the element there.
          if (!closestCluster.equals(originCluster)) {
            iter.remove();
            closestCluster.elements.add(element);
          }
        }
      }
    }
  }
}
