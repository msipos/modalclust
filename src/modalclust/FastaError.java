package modalclust;

public class FastaError extends RuntimeException {

  public FastaError(String message) {
    super(message);
  }

  public FastaError(String message, Throwable cause) {
    super(message, cause);
  }

}
