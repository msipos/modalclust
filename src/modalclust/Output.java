package modalclust;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import modalclust.seq.Sequence;

public class Output {
  public static void writeRabund(String filename, List<List<Sequence>> flatClusters) throws IOException {
    FileWriter writer = new FileWriter(filename);
    writer.write(String.format("%f\t%d\t", ModalClust.params.radius, flatClusters.size()));
    for (List<Sequence> cluster : flatClusters) {
      writer.write(String.format("%d\t", cluster.size()));
    }
    writer.close();
  }


  public static void writeList(String filename, List<List<Sequence>> flatClusters) throws IOException {
    FileWriter writer = new FileWriter(filename);
    writer.write(String.format("%f\t%d\t", ModalClust.params.radius, flatClusters.size()));
    for (List<Sequence> cluster : flatClusters) {
      boolean first = false;
      for (Sequence seq : cluster) {
        if (first) writer.write(',');
        first = true;
        writer.write(seq.getId());
      }
      writer.write(' ');
    }
    writer.close();
  }


  public static void writeSeeds(String filename, Collection<Sequence> seeds) throws IOException {
    FileWriter writer = new FileWriter(filename);
    int n = 1;
    for (final Sequence seed : seeds) {
      writer.write(">OTU " + n + "\n");
      writer.write(seed.snippet.getSequence() + "\n");
      n++;
    }
    writer.close();
  }


  public static void writeHtml(String filename,
                               List<Sequence> sequenceList,
                               List<Sequence> duplSequenceList,
                               List<List<Sequence>> flatClusters) throws IOException {
    FileWriter writer = new FileWriter(filename);
    writer.write("<html><head><title>clustering results</title></head><body>");
    writer.write("<h1>Clustering results</h2>");
    writer.write("Time of clustering run: " + new Date());
    writer.write("Input filename: '" + ModalClust.params.fastaFile.getAbsolutePath() + "'<br/>");
    if (ModalClust.params.dictFile != null) {
      writer.write("Dictionary filename: '" + ModalClust.params.dictFile.getAbsolutePath() + "'<br/>");
    }
    writer.write("This file: 'cluster.html'<br />");
    writer.write("Files for use with mothur: 'cluster.list' and 'cluster.rabund'<br /><br />");
    writer.write("Number of sequences: " + sequenceList.size() + "<br />");
    writer.write("Number of unique sequences: " + duplSequenceList.size() + "<br /><br />");
    writer.write("Clustering radius: " + ModalClust.params.radius + "<br />");
    writer.write("Number of clusters: " + flatClusters.size());

    writer.write("<h2>Rank abundance data</h2>");
    writer.write("<table border=\"1\">");
    writer.write("<tr><th>Cluster rank</th><th>Cluster abundance</th></tr>");
    int i = 0;
    for (List<Sequence> cluster : flatClusters) {
      i++;
      writer.write(String.format("<tr><td>%d</td><td>%d</td></tr>\n", i, cluster.size()));
    }
    writer.write("</table>");

    writer.write("<h2>Cluster data</h2>");
    writer.write("<table border=\"1\">");
    writer.write("<tr><th>Cluster rank</th><th>Sequences belonging to cluster</th></tr>");
    i = 0;
    for (List<Sequence> cluster : flatClusters) {
      Collections.sort(cluster);

      i++;
      writer.write(String.format("\n<tr><td>%d</td><td>", i));
      int j = 1;
      for (Sequence seq : cluster) {
        writer.write(seq.getId());
        if (j % 5 == 0) writer.write("<br />\n");
        else writer.write(" &nbsp; ");
        j++;
      }
      writer.write(' ');
      writer.write("</td></tr>");
    }
    writer.write("</table>");
    writer.write("</body></html>");
    writer.close();
  }
}
