package modalclust;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import java.io.*;
import java.util.*;
import modalclust.seq.Sequence;
import modalclust.seq.Snippet;

public class ModalClust {
  public static final ModalClustParams params = new ModalClustParams();
  private static Clustering.DistanceMetric<Sequence> distanceMetric = new MothurDistanceMetric();


  public static Clustering<Sequence> cluster(List<Sequence> l) {
    final Clustering<Sequence> clustering = new Clustering<Sequence>();

    // Sort and reverse the duplication sequences
    Collections.sort(l);
    Collections.reverse(l);

    clustering.radiusCluster(l, distanceMetric, (float) params.radius, params.noOptimization);

    return clustering;
  }


  private static List<List<Sequence>> flatten(Collection<Set<Sequence>> clusters) {
    List<List<Sequence>> results = new ArrayList<List<Sequence>>();

    for (Set<Sequence> cluster : clusters) {
      List<Sequence> newCluster = new ArrayList<Sequence>();
      for (Sequence dseq : cluster) {
        for (String id : dseq.ids) {
          newCluster.add(new Sequence(dseq.snippet, id));
        }
      }
      results.add(newCluster);
    }

    return results;
  }


  /** Load a list of sequences from a file and check that it is reasonable. */
  public static List<Sequence> loadSequences(File fastaFile) {
    final String filename = fastaFile.getAbsolutePath();
    List<Sequence> list = null;
    try {
      list = Fasta.readFasta(fastaFile);
    } catch (FileNotFoundException ex) {
      System.err.printf("ERROR: Could not find '%s'%n", filename);
      System.exit(1);
    } catch (FastaError ex) {
      System.err.printf("ERROR: While parsing '%s': %s%n", filename, ex.getMessage());
      System.exit(1);
    }

    // Check that input is not empty
    if (list.isEmpty()) {
      System.err.printf("ERROR: Empty file '%s'%n", filename);
      System.exit(1);
    }

    // Check that all sizes are the same and all sequences are reasonable
    {
      final Sequence first = list.get(0);
      final int size = first.snippet.size();
      if (size == 0) {
        System.err.printf("ERROR: sequence '%s' has length 0%n", first.getId());
        System.exit(1);
      }
      for (Sequence seq : list) {
        if (seq.snippet.size() != size) {
          System.err.printf("ERROR: sequence '%s' has length %d but '%s' has different length %d%n",
                            first.getId(), size, seq.getId(), seq.snippet.size());
          System.exit(1);
        }
      }
    }

    return list;
  }


  public static void main(String[] args) throws IOException {
    ModalClustCLI.parseArguments(args);
    if (params.verbose) Log.destination = Log.Destination.STDERR;

    // Load, dereplicate, cluster and flatten.
    final List<Sequence> list = loadSequences(params.fastaFile);
    final List<Sequence> dlist = Dereplicate.dereplicate(params, list);

    if (params.classFile != null) {
      // We have a classification file. First split according to classification, then cluster:
      // TODO.
      // Load the classification file:
      // TODO.
      System.err.println("Sorry! Not implemented yet.");
      System.exit(1);
    }

    if (params.clustQualityFile != null) {
      System.err.println("Will not perform clustering.  Instead I will compute Calinski-Harabasz metric for:");
      System.err.println("'" + params.clustQualityFile.getAbsolutePath() + "'");
      ClusteringQuality.calculateCH(dlist, params.clustQualityFile);
      System.exit(0);
    }

    Log.log("Starting clustering...");

    final Clustering<Sequence> clustering = cluster(dlist);

    List<List<Sequence>> flatClusters = flatten(clustering.getClusters());

    // Sort the flattened list (for output of .rabund)
    Collections.sort(flatClusters, new Comparator<List<Sequence>>() {
      public int compare(List<Sequence> o1, List<Sequence> o2) {
        return o2.size() - o1.size();
      }
    });

    Log.log("Did %d distance calculations (of which %d heuristic = %f %%).", DistanceMetrics.numAllCalculations,
                                                                             DistanceMetrics.numHeuristics,
            100.0*((double) DistanceMetrics.numHeuristics) / ((double) DistanceMetrics.numAllCalculations) );
    Output.writeRabund(params.filePrefix + ".rabund", flatClusters);
    Output.writeList(params.filePrefix + ".list", flatClusters);
    Output.writeSeeds(params.filePrefix + ".seeds", clustering.getSeeds());
    Output.writeHtml(params.filePrefix + ".html", list, dlist, flatClusters);
  }
}
