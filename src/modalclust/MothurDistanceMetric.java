package modalclust;

import modalclust.seq.Snippet;
import modalclust.seq.Sequence;
public class MothurDistanceMetric extends Clustering.DistanceMetric<Sequence> {
  @Override public float distance(Sequence e1, Sequence e2, float maxRadius) {
    final Snippet a = e1.snippet, b = e2.snippet;
    return DistanceMetrics.distanceMothur(a, b, maxRadius);
  }
}
